package com.learnJava;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveDuplicates {
	public static void main(String[] args) {
		/**
		 * Remove duplicate interger values
		 */
		List<Integer> intergerList = Arrays.asList(1, 2, 3, 3, 4, 5, 6, 5, 4, 8, 9, 10);
		List<Object> uniqueList = intergerList.stream().distinct().collect(Collectors.toList());
		System.out.println("UniqueList :" + uniqueList);
	}

}
