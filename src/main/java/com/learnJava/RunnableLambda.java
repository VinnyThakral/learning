package com.learnJava;

public class RunnableLambda {
	/*
	 * Assigning value to a variable
	 */
	public static void main(String[] args) {
		Runnable runnablelambda = () -> {
			System.out.println("Inside lambda 1");
		};
		new Thread(runnablelambda).start();

		Runnable runnnableLambda2 = () -> System.out.println("Single statement doenot require curly braces");
		new Thread(runnnableLambda2).start();

		new Thread(() -> System.out.println("Inside Thread creation")).start();
	}

}
