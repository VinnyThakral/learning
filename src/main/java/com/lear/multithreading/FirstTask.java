package com.lear.multithreading;

import java.util.concurrent.TimeUnit;

public class FirstTask {
	public static void main(String[] args) {
		new Task();
		new Task();
	}
}

class Task extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("TICK TICK " + i);
			try {
				TimeUnit.MILLISECONDS.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public Task() {
		
		this.start();
	}

}
